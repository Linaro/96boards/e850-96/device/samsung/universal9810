#!/bin/bash
adb reboot bootloader

fastboot flash el3_mon out/target/product/universal9810/el3_mon.img
fastboot flash epbl out/target/product/universal9810/epbl.img
fastboot flash bootloader out/target/product/universal9810/bootloader.img

fastboot flash dtb out/target/product/universal9810/dtb.img
fastboot flash dtbo out/target/product/universal9810/dtbo.img
fastboot flash boot out/target/product/universal9810/boot.img

fastboot flash recovery out/target/product/universal9810/recovery.img
fastboot flash vbmeta out/target/product/universal9810/vbmeta.img

fastboot -w
fastboot reboot fastboot

fastboot flash vendor out/target/product/universal9810/vendor.img
fastboot flash system out/target/product/universal9810/system.img
fastboot reboot
