# Copyright (C) 2011 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# This file is the build configuration for a full Android
# build for universal9810 hardware. This cleanly combines a set of
# device-specific aspects (drivers) with a device-agnostic
# product configuration (apps). Except for a few implementation
# details, it only fundamentally contains two inherit-product
# lines, full and universal9810, hence its name.
#

ifeq ($(TARGET_PRODUCT),full_universal9810)
$(warning #### TARGET_PRODUCT = $(TARGET_PRODUCT))
# Live Wallpapers
PRODUCT_PACKAGES += \
        LiveWallpapers \
        LiveWallpapersPicker \
        MagicSmokeWallpapers \
        VisualizationWallpapers \
        librs_jni

PRODUCT_PROPERTY_OVERRIDES := \
        net.dns1=8.8.8.8 \
        net.dns2=8.8.4.4

include vendor/samsung/hardware/wifi/base_wifi.mk
include vendor/samsung/hardware/wifi/broadcom/bcm4361/wifi.mk

# Inherit from those products. Most specific first.
$(call inherit-product, device/samsung/universal9810/device.mk)
# $(call inherit-product, build/target/product/aosp_arm64.mk)

PRODUCT_NAME := full_universal9810
PRODUCT_DEVICE := universal9810
PRODUCT_BRAND := Exynos
PRODUCT_MODEL := Full Android on SMDK9810
PRODUCT_MANUFACTURER := Samsung Electronics Co., Ltd.
TARGET_LINUX_KERNEL_VERSION := 4.9
endif
