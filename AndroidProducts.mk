
PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/aosp_universal9810.mk \
    $(LOCAL_DIR)/full_universal9810.mk \
    $(LOCAL_DIR)/full_universal9810_q_launching.mk \
    $(LOCAL_DIR)/full_universal9810_q_launching_mali.mk

COMMON_LUNCH_CHOICES := \
    full_universal9810-eng \
    full_universal9810-userdebug \
    full_universal9810-user \
    full_universal9810_q_launching-eng \
    full_universal9810_q_launching-userdebug \
    full_universal9810_q_launching-user \
    full_universal9810_q_launching_mali-userdebug
