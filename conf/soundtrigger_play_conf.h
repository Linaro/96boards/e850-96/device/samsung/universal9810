/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EXYNOS_SOUNDTRIGGER_PLAY_CONF_H__
#define __EXYNOS_SOUNDTRIGGER_PLAY_CONF_H__

/********************************************************************/
/** ALSA Framework Sound Card & Sound Device Information            */
/**                                                                 */
/** You can find Sound Device Name from /dev/snd.                   */
/** Sound Device Name consist of Card Number & Device Number.       */
/**                                                                 */
/********************************************************************/
#define PLAYBACK_OUT_DEVICE_NODE       1    //3
#define MAIN_SPKC_CONTROL_COUNT         8
#define MAIN_SPKC_CONTROL_ARRAY_COUNT   1

/* playback Mixer control values */
/* FIXME : Double check this values */

char *headset_spk_ctlname[] = {
    "ABOX SPUS OUT2",
    "ABOX SIFS0 Rate",
    "ABOX SIFS0 Width",
    "ABOX SIFS0 Channel",
    "ABOX UAIF5 SPK",
    "ABOX UAIF5 Rate",
    "ABOX UAIF5 Width",
    "ABOX UAIF5 Channel",
};

int headset_spk_ctlvalue[] = {
    1,      //"ABOX SPUS OUT2" SIFS0
    16000,  //"ABOX SIFS0 Rate" 16000
    16,     //"ABOX SIFS0 Width" 16
    2,      //"ABOX SIFS0 Channel" 2
    1,      //"ABOX UAIF5 SPK" SIFS0
    16000,  //"ABOX UAIF5 Rate" 16000
    16,     //"ABOX UAIF5 Width" 16
    2,      //"ABOX UAIF5 Channel" 2
};

int headset_spk_not_ctlvalue[] = {
    0,      //"ABOX SPUS OUT2" RESERVED
    16000,  //"ABOX SIFS0 Rate" 16000
    16,     //"ABOX SIFS0 Width" 16
    2,      //"ABOX SIFS0 Channel" 2
    0,      //"ABOX UAIF5 SPK" RESERVED
    16000,  //"ABOX UAIF5 Rate" 16000
    16,     //"ABOX UAIF5 Width" 16
    2,      //"ABOX UAIF5 Channel" 2
};

char *headset_spk_ctlname_arry[] = {
    "HPOUT2 Digital Switch",
};

int *headset_spk_ctlvalue_arry[] = {
    (int[]){1,1},   //"HPOUT2 Digital Switch",
};

int *headset_spk_not_ctlvalue_arry[] = {
    (int[]){0,0},   //"HPOUT2 Digital Switch",
};

int headset_spk_ctlvalue_cnt_arry[] = {
    2,      //"HPOUT2 Digital Switch",
};
#endif  // __EXYNOS_SOUNDTRIGGER_PLAY_CONF_H__