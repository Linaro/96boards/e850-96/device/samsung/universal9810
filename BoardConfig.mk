#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Should be uncommented after fixing vndk-sp violation is fixed.
PRODUCT_FULL_TREBLE_OVERRIDE := true

TARGET_LINUX_KERNEL_VERSION := 4.9
TARGET_BOARD_INFO_FILE := device/samsung/universal9810/board-info.txt

# HACK : To fix up after bring up multimedia devices.
TARGET_DEVICE_NAME := universal9810
TARGET_SOC_NAME := exynos
TARGET_SOC := exynos9810
TARGET_SOC_BASE := exynos9810

TARGET_ARCH := arm64
TARGET_ARCH_VARIANT := armv8-a
TARGET_CPU_ABI := arm64-v8a
TARGET_CPU_VARIANT := cortex-a53

TARGET_2ND_ARCH := arm
TARGET_2ND_ARCH_VARIANT := armv8-a
TARGET_2ND_CPU_ABI := armeabi-v7a
TARGET_2ND_CPU_ABI2 := armeabi
TARGET_2ND_CPU_VARIANT := cortex-a53
TARGET_CPU_SMP := true

TARGET_NO_BOOTLOADER := true
TARGET_NO_KERNEL := true
TARGET_NO_RADIOIMAGE := true
TARGET_BOARD_PLATFORM := universal9810
TARGET_BOOTLOADER_BOARD_NAME := exynos9810

# SMDK common modules
BOARD_SMDK_COMMON_MODULES := liblight

OVERRIDE_RS_DRIVER := libRSDriverArm.so
BOARD_EGL_CFG := device/samsung/universal9810/conf/egl.cfg
#BOARD_USES_HGL := true
USE_OPENGL_RENDERER := true
NUM_FRAMEBUFFER_SURFACE_BUFFERS := 3
BOARD_USES_EXYNOS5_COMMON_GRALLOC := true
BOARD_USES_EXYNOS_GRALLOC_VERSION := 3
BOARD_USES_ALIGN_RESTRICTION := true

# Graphics
BOARD_USES_EXYNOS_DATASPACE_FEATURE := true

# Storage options
BOARD_USES_SDMMC_BOOT := false
BOARD_USES_UFS_BOOT := true

ifeq ($(ENABLE_Q_FEATURE),true)
BOARD_KERNEL_CMDLINE += androidboot.selinux=enforce
else
BOARD_KERNEL_CMDLINE += androidboot.selinux=permissive
endif
BOARD_USES_VENDORIMAGE := true
TARGET_COPY_OUT_VENDOR := vendor
BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_VENDORIMAGE_PARTITION_SIZE := 314572800

TARGET_USERIMAGES_USE_EXT4 := true
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 2662400000
BOARD_USERDATAIMAGE_PARTITION_SIZE := 13107200000
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_CACHEIMAGE_PARTITION_SIZE := 69206016
BOARD_FLASH_BLOCK_SIZE := 4096
BOARD_MOUNT_SDCARD_RW := true


ifeq ($(ENABLE_Q_FEATURE),true)
####
TARGET_NO_KERNEL := false
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x02800000
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x02800000
BOARD_DTBOIMG_PARTITION_SIZE := 0x00100000

# TODO: Disable recovery part once A/B is enabled
BOARD_AVB_RECOVERY_KEY_PATH := device/samsung/universal9810/avbkey_rsa4096.pem
BOARD_AVB_RECOVERY_ALGORITHM := SHA256_RSA4096
BOARD_AVB_RECOVERY_ROLLBACK_INDEX := 0
BOARD_AVB_RECOVERY_ROLLBACK_INDEX_LOCATION := 0

# Dynamic Partitions options
BOARD_SUPER_PARTITION_SIZE := 4194304000

# Configuration for dynamic partitions.
BOARD_SUPER_PARTITION_GROUPS := group_basic
BOARD_GROUP_BASIC_SIZE := 4190109696
ifeq ($(WITH_ESSI),true)
BOARD_GROUP_BASIC_PARTITION_LIST := vendor
else
BOARD_GROUP_BASIC_PARTITION_LIST := system vendor
endif
endif

# WIFI related definition
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
WPA_SUPPLICANT_VERSION      := VER_0_8_X
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_bcmdhd
BOARD_HOSTAPD_DRIVER        := NL80211
BOARD_HOSTAPD_PRIVATE_LIB   := lib_driver_cmd_bcmdhd
BOARD_WLAN_DEVICE           := bcmdhd
WIFI_DRIVER_FW_PATH_PARAM   := "/sys/module/dhd/parameters/firmware_path"
#WIFI_DRIVER_MODULE_PATH     := "/lib/modules/dhd.ko"
WIFI_DRIVER_MODULE_NAME     := "dhd"
WIFI_DRIVER_MODULE_ARG      := "firmware_path=/system/vendor/etc/wifi/bcmdhd_sta.bin_b0 nvram_path=/system/vendor/etc/wifi/nvram_net.txt"
WIFI_DRIVER_MODULE_AP_ARG   := "firmware_path=/system/vendor/etc/wifi/bcmdhd_apsta.bin nvram_path=/system/vendor/etc/wifi/nvram_net.txt"
WIFI_DRIVER_MODULE_P2P_ARG  := "firmware_path=/system/vendor/etc/wifi/bcmdhd_sta.bin_b0 nvram_path=/system/vendor/etc/wifi/nvram_net.txt"
#WIFI_DRIVER_FW_PATH_STA     := "/system/vendor/etc/wifi/bcmdhd_sta.bin"
#WIFI_DRIVER_FW_PATH_P2P     := "/system/vendor/etc/wifi/bcmdhd_sta.bin_b0"
#WIFI_DRIVER_FW_PATH_AP      := "/system/vendor/etc/wifi/bcmdhd_apsta.bin"
#WIFI_DRIVER_FW_PATH_MFG     := "/system/vendor/etc/wifi/bcmdhd_mfg.bin_b0"

# We do not want to overwrite WLAN configurations in universalxxxx/BoardConfig.mk file.
#
# # Choose the vendor of WLAN for wlan_mfg and wifi.c
# # 1. Broadcom
# # 2. Atheros
# # 3. TI
# # 4. Qualcomm
 WLAN_VENDOR = 1

# # Choose the WLAN chipset
# # broadcom: bcm4329, bcm4330, bcm4334, bcm43241, bcm4335, bcm4339, bcm4354
# # atheros: ar6003x, ar6004, ar6005, ar603x
 WLAN_CHIP := bcm4361

# # Choose the type of WLAN chipset
# # CoB type: COB, Module type: MODULE
 WLAN_CHIP_TYPE := MODULE

########################
# Video Codec
########################
# 0. Default C2
BOARD_USE_DEFAULT_SERVICE := true

# 2. Exynos OMX
BOARD_USE_DMA_BUF := true
BOARD_USE_NON_CACHED_GRAPHICBUFFER := true
BOARD_USE_GSC_RGB_ENCODER := true
BOARD_USE_CSC_HW := false
BOARD_USE_S3D_SUPPORT := false
BOARD_USE_DEINTERLACING_SUPPORT := true
BOARD_USE_HEVCENC_SUPPORT := true
BOARD_USE_HEVC_HWIP := false
BOARD_USE_VP9DEC_SUPPORT := true
BOARD_USE_VP9ENC_SUPPORT := true
BOARD_USE_WFDENC_SUPPORT := true
BOARD_USE_CUSTOM_COMPONENT_SUPPORT := true
BOARD_USE_VIDEO_EXT_FOR_WFD_HDCP := true
BOARD_USE_SINGLE_PLANE_IN_DRM := true
########################

#
# AUDIO & VOICE
#
BOARD_USES_GENERIC_AUDIO := false

# Primary AudioHAL Configuration
BOARD_USE_COMMON_AUDIOHAL := true
BOARD_USE_CALLIOPE_AUDIOHAL := false
BOARD_USE_AUDIOHAL := true

# Audio Feature Configuration
BOARD_USE_OFFLOAD_AUDIO := true
BOARD_USE_OFFLOAD_EFFECT := false
BOARD_USE_BTA2DP_OFFLOAD := false

# SoundTriggerHAL Configuration
BOARD_USE_SOUNDTRIGGER_HAL := false
BOARD_USE_SOUNDTRIGGER_HAL_MMAP := false

# CAMERA
BOARD_BACK_CAMERA_ROTATION := 90
BOARD_FRONT_CAMERA_ROTATION := 270
BOARD_SECURE_CAMERA_ROTATION := 0
BOARD_BACK_CAMERA_SENSOR := SENSOR_NAME_SAK2L3
BOARD_FRONT_CAMERA_SENSOR := SENSOR_NAME_S5K3H1
BOARD_SECURE_CAMERA_SENSOR := SENSOR_NAME_S5K5F1

#BOARD_CAMERA_USES_DUAL_CAMERA := false
#BOARD_BACK_1_CAMERA_SENSOR := SENSOR_NAME_NOTHING
#BOARD_FRONT_1_CAMERA_SENSOR := SENSOR_NAME_NOTHING

# HWComposer
BOARD_HWC_VERSION := libhwc2.1
TARGET_RUNNING_WITHOUT_SYNC_FRAMEWORK := false
BOARD_HDMI_INCAPABLE := true
TARGET_USES_HWC2 := true
HWC_SKIP_VALIDATE := true
BOARD_USES_DISPLAYPORT := true
BOARD_USES_EXYNOS_AFBC_FEATURE := true
BOARD_USES_HDRUI_GLES_CONVERSION := false

SOONG_CONFIG_NAMESPACES += libacryl
SOONG_CONFIG_libacryl += default_compositor \
                         default_scaler \
                         default_blter \
                         g2d9810_hdr_plugin
SOONG_CONFIG_libacryl_default_compositor := fimg2d_9810
SOONG_CONFIG_libacryl_default_scaler := mscl_9810
SOONG_CONFIG_libacryl_default_blter := fimg2d_9810_blter
SOONG_CONFIG_libacryl_g2d9810_hdr_plugin := libacryl_plugin_slsi_hdr10

# HWCServices
BOARD_USES_HWC_SERVICES := true

# WiFiDisplay
BOARD_USES_VIRTUAL_DISPLAY := true
BOARD_USES_DISABLE_COMPOSITIONTYPE_GLES := true
BOARD_USES_SECURE_ENCODER_ONLY := true

# SCALER
BOARD_USES_DEFAULT_CSC_HW_SCALER := true
BOARD_DEFAULT_CSC_HW_SCALER := 4
BOARD_USES_SCALER_M2M1SHOT := true
BOARD_HAS_SCALER_ALIGN_RESTRICTION := true

# Device Tree
BOARD_USES_DT := true

# PLATFORM LOG
TARGET_USES_LOGD := true

# LIBHWJPEG
TARGET_USES_UNIVERSAL_LIBHWJPEG := true
LIBHWJPEG_HWSCALER_ID := 0

#Keymaster
ifeq ($(ENABLE_Q_FEATURE),true)
BOARD_USES_KEYMASTER_VER4 := true
else
BOARD_USES_KEYMASTER_VER1 := true
endif

#FMP
BOARD_USES_FMP_DM_CRYPT := true
BOARD_USES_FMP_FSCRYPTO := true

# SKIA
#BOARD_USES_SKIA_MULTITHREADING := true
#BOARD_USES_FIMGAPI_V5X := true

# SELinux Platform Private policy for exynos
BOARD_PLAT_PRIVATE_SEPOLICY_DIR := device/samsung/sepolicy/private

# SELinux Platform Public policy for exynos
BOARD_PLAT_PUBLIC_SEPOLICY_DIR := device/samsung/sepolicy/public

ifeq ($(ENABLE_Q_FEATURE),true)
# SELinux policies
VENDOR_SEPOLICY := device/samsung/sepolicy/common \
		   device/samsung/universal9810/sepolicy_29
else
# SELinux policies
VENDOR_SEPOLICY := device/samsung/sepolicy/common \
		   device/samsung/universal9810/sepolicy
endif

ifeq (, $(findstring $(VENDOR_SEPOLICY), $(BOARD_VENDOR_SEPOLICY_DIRS)))
	BOARD_VENDOR_SEPOLICY_DIRS += $(VENDOR_SEPOLICY)
endif

# SECCOMP Policy
BOARD_SECCOMP_POLICY = device/samsung/universal9810/seccomp_policy

# SELinux Platform Private policy for exynos
BOARD_PLAT_PRIVATE_SEPOLICY_DIR := device/samsung/sepolicy/private

#CURL
BOARD_USES_CURL := true

# VISION
# Exynos vision framework (EVF)
#TARGET_USES_EVF := true
# HW acceleration
#TARGET_USES_VPU_KERNEL := true
#TARGET_USES_SCORE_KERNEL := true
#TARGET_USES_CL_KERNEL := false

# exynos RIL
TARGET_EXYNOS_RIL_SOURCE := true

# SENSOR HUB
BOARD_USES_EXYNOS_SENSORS_DUMMY := true

# GNSS
BOARD_USES_EXYNOS_GNSS_DUMMY := true

TARGET_BOARD_KERNEL_HEADERS := hardware/samsung_slsi/exynos/kernel-4.9-headers/kernel-headers

#VNDK
BOARD_PROPERTY_OVERRIDES_SPLIT_ENABLED := true
BOARD_VNDK_VERSION := current

# H/W align restriction of MM IPs
BOARD_EXYNOS_S10B_FORMAT_ALIGN := 64

# Extra mount point
BOARD_ROOT_EXTRA_SYMLINKS += /mnt/vendor/efs:/efs
BOARD_ROOT_EXTRA_SYMLINKS += /mnt/vendor/persist:/persist

# Enable AVB2.0
BOARD_AVB_ENABLE := true
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x02000000
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x02800000
BOARD_DTBOIMG_PARTITION_SIZE := 0x00100000
BOARD_AVB_ALGORITHM := SHA256_RSA4096
BOARD_AVB_KEY_PATH := device/samsung/universal9810/avbkey_rsa4096.pem

# libExynosGraphicbuffer
SOONG_CONFIG_NAMESPACES += exynosgraphicbuffer
SOONG_CONFIG_exynosgraphicbuffer := \
	gralloc_version

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),3)
SOONG_CONFIG_exynosgraphicbuffer_gralloc_version := three
endif

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),4)
SOONG_CONFIG_exynosgraphicbuffer_gralloc_version := four
endif

ifeq ($(MALI_ON_KEYSTONE),true)
# Allow deprecated BUILD_ module types required to build Mali UMD
BUILD_BROKEN_USES_BUILD_COPY_HEADERS := true
BUILD_BROKEN_USES_BUILD_HOST_EXECUTABLE := true
BUILD_BROKEN_USES_BUILD_HOST_SHARED_LIBRARY := true
BUILD_BROKEN_USES_BUILD_HOST_STATIC_LIBRARY := true
BUILD_BROKEN_MISSING_REQUIRED_MODULES := true
endif

# Enable Renderscript for new ddk r38p1
#include vendor/arm/mali_src/android/renderscript.device.mk
SOONG_CONFIG_NAMESPACES += mali_rs
SOONG_CONFIG_mali_rs += \
	gpu_arch

SOONG_CONFIG_mali_rs_gpu_arch := BIFROST

# USB (USB gadgethal)
SOONG_CONFIG_NAMESPACES += usbgadgethal
SOONG_CONFIG_usbgadgethal:= exynos_product
SOONG_CONFIG_usbgadgethal_exynos_product := default

BOARD_USES_GRALLOC_ION_SYNC := true
