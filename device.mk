#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

ifeq ($(ENABLE_Q_FEATURE),true)
PRODUCT_SHIPPING_API_LEVEL := 29
else
PRODUCT_SHIPPING_API_LEVEL := 26
endif

BOARD_PREBUILTS := device/samsung/$(TARGET_PRODUCT:full_%=%)-prebuilts
ifneq ($(wildcard $(BOARD_PREBUILTS)),)

BOARD_PREBUILT_DTBOIMAGE := $(BOARD_PREBUILTS)/dtbo.img
BOARD_PREBUILT_DTB := $(BOARD_PREBUILTS)/dtb.img
BOARD_PREBUILT_BOOTLOADER_IMG := $(BOARD_PREBUILTS)/bootloader.img
BOARD_PREBUILT_EL3IMAGE := $(BOARD_PREBUILTS)/el3_mon.img
BOARD_PREBUILT_EPBLIMAGE := $(BOARD_PREBUILTS)/epbl.img

PRODUCT_COPY_FILES += $(foreach image,\
	$(filter-out $(BOARD_PREBUILT_DTBOIMAGE) $(BOARD_PREBUILT_BOOTLOADER_IMG) $(BOARD_PREBUILT_DTB) $(BOARD_PREBUILT_EL3IMAGE) $(BOARD_PREBUILT_EPBLIMAGE), $(wildcard $(BOARD_PREBUILTS)/*)),\
	$(image):$(PRODUCT_OUT)/$(notdir $(image)))

else #BOARD_PREBUILTS

ifeq ($(TARGET_PREBUILT_KERNEL),)
INSTALLED_KERNEL_TARGET := vendor/samsung_slsi/exynos9810/prebuilts/kernel
BOARD_PREBUILT_KERNELIMAGE := vendor/samsung_slsi/exynos9810/prebuilts/kernel
else
INSTALLED_KERNEL_TARGET := $(TARGET_PREBUILT_KERNEL)
endif

BOARD_PREBUILT_DTBOIMAGE := vendor/samsung_slsi/exynos9810/prebuilts/dtbo.img
BOARD_PREBUILT_DTB := vendor/samsung_slsi/exynos9810/prebuilts/universal9810.dtb
BOARD_PREBUILT_BOOTLOADER_IMG := vendor/samsung_slsi/exynos9810/prebuilts/bootloader.img
BOARD_PREBUILT_EL3IMAGE := vendor/samsung_slsi/exynos9810/prebuilts/el3_mon.img
BOARD_PREBUILT_EPBLIMAGE := vendor/samsung_slsi/exynos9810/prebuilts/epbl.img

PRODUCT_COPY_FILES += \
    $(INSTALLED_KERNEL_TARGET):kernel \

endif #BOARD_PREBUILTS

include $(LOCAL_PATH)/BoardConfig.mk

ifeq ($(ENABLE_Q_FEATURE),true)
# Enable Dynamic Partitions
PRODUCT_USE_DYNAMIC_PARTITIONS := true
endif

# From system.property
PRODUCT_PROPERTY_OVERRIDES += \
    ro.arch=exynos9810 \
    persist.demo.hdmirotationlock=false \
    dev.usbsetting.embedded=on
    # ro.debug_level=0x494d
    # ro.telephony.default_network=9 \
    # rild.libpath=/system/lib64/libsec-ril.so \
    # rild.libargs=-d /dev/umts_ipc0

# Device Manifest, Device Compatibility Matrix for Treble
ifeq ($(ENABLE_Q_FEATURE),true)
DEVICE_MANIFEST_FILE := \
	device/samsung/universal9810/manifest_api29.xml

DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
	device/samsung/universal9810/framework_compatibility_matrix_api29.xml
else
DEVICE_MANIFEST_FILE := \
	device/samsung/universal9810/manifest.xml

DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
	device/samsung/universal9810/framework_compatibility_matrix.xml
endif

DEVICE_MATRIX_FILE := \
	device/samsung/universal9810/compatibility_matrix.xml

# These are for the multi-storage mount.
ifeq ($(BOARD_USES_SDMMC_BOOT),true)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal9810/overlay-sdboot
else
ifeq ($(BOARD_USES_UFS_BOOT),true)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal9810/overlay-ufsboot
else
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal9810/overlay-emmcboot
endif
endif
DEVICE_PACKAGE_OVERLAYS += device/samsung/universal9810/overlay

ifeq ($(ENABLE_Q_FEATURE),true)
CONF_NAME := conf_api29
else
CONF_NAME := conf
endif

# Init files
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/$(CONF_NAME)/init.samsungexynos9810.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.samsungexynos9810.rc \
	device/samsung/universal9810/$(CONF_NAME)/init.samsungexynos9810.usb.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.samsungexynos9810.usb.rc \
	device/samsung/universal9810/$(CONF_NAME)/ueventd.samsungexynos9810.rc:$(TARGET_COPY_OUT_VENDOR)/ueventd.rc

ifeq ($(ENABLE_Q_FEATURE),true)
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/$(CONF_NAME)/init.recovery.exynos9810.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.recovery.samsungexynos9810.rc \
	device/samsung/universal9810/$(CONF_NAME)/init.recovery.exynos9810.rc:root/init.recovery.samsungexynos9810.rc

ifeq ($(BOARD_USES_SDMMC_BOOT),true)
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810.sdboot:$(TARGET_COPY_OUT_RAMDISK)/fstab.samsungexynos9810 \
	device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810.sdboot:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.samsungexynos9810
TARGET_RECOVERY_FSTAB := device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810.sdboot
else ifeq ($(BOARD_USES_UFS_BOOT),true)
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810:$(TARGET_COPY_OUT_RAMDISK)/fstab.samsungexynos9810 \
	device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.samsungexynos9810
TARGET_RECOVERY_FSTAB := device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810
else
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810.emmc:$(TARGET_COPY_OUT_RAMDISK)/fstab.samsungexynos9810 \
	device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810.emmc:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.samsungexynos9810
TARGET_RECOVERY_FSTAB := device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810.emmc
endif

else # ENABLE_Q_FEATURE

ifeq ($(BOARD_USES_SDMMC_BOOT),true)
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810.sdboot:root/fstab.samsungexynos9810 \
	device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810.sdboot:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.samsungexynos9810
else ifeq ($(BOARD_USES_UFS_BOOT),true)
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810:root/fstab.samsungexynos9810 \
	device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.samsungexynos9810
else
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810.emmc:root/fstab.samsungexynos9810 \
	device/samsung/universal9810/$(CONF_NAME)/fstab.samsungexynos9810.emmc:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.samsungexynos9810
endif

endif # ENABLE_Q_FEATURE

# PRODUCT_COPY_FILES += \
	device/samsung/universal9810/manifest.xml:$(TARGET_COPY_OUT_VENDOR)/manifest.xml

# Support devtools
PRODUCT_PACKAGES += \
	Development

# Filesystem management tools
PRODUCT_PACKAGES += \
	e2fsck

# RPMB TA
ifeq ($(ENABLE_Q_FEATURE),true)
PRODUCT_PACKAGES += \
        tlrpmb_api29
#tlrpmb_api29
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/api29/09090000070100010000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/09090000070100010000000000000000.tlbin
else
PRODUCT_PACKAGES += \
        tlrpmb
#tlrpmb
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/09090000070100010000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/09090000070100010000000000000000.tlbin
endif

# Sensor HAL
PRODUCT_PACKAGES += \
	android.hardware.sensors@1.0-impl \
	android.hardware.sensors@1.0-service \
	sensors.$(TARGET_SOC)

# USB HAL
PRODUCT_PACKAGES += \
	android.hardware.usb@1.1 \
	android.hardware.usb@1.1-service

ifeq ($(ENABLE_Q_FEATURE),true)
# Fastboot HAL
PRODUCT_PACKAGES += \
       fastbootd \
       android.hardware.fastboot@1.1\
       android.hardware.fastboot@1.1-impl-mock.exynos9810
endif

# Power HAL
PRODUCT_PACKAGES += \
	android.hardware.power@1.0-impl \
	android.hardware.power@1.0-service \
	power.$(TARGET_SOC)

# Thermal HAL
PRODUCT_PACKAGES += \
	android.hardware.thermal@2.0-impl \
	android.hardware.thermal@2.0-service.exynos \
	thermal.$(TARGET_SOC)

ifeq ($(ENABLE_Q_FEATURE),true)
#Health 2.0 HAL
PRODUCT_PACKAGES += \
	android.hardware.health@2.0-service
endif

# configStore HAL
PRODUCT_PACKAGES += \
    android.hardware.configstore@1.0-service \
    android.hardware.configstore@1.0-impl

#
# Audio HALs
#

# Audio Configurations
USE_LEGACY_LOCAL_AUDIO_HAL := false
USE_XML_AUDIO_POLICY_CONF := 1

ifeq ($(ENABLE_Q_FEATURE),true)
# Audio HAL Server & Default Implementations
PRODUCT_PACKAGES += \
    android.hardware.audio@2.0-service \
    android.hardware.audio@5.0-impl \
    android.hardware.audio.effect@5.0-impl \
    android.hardware.soundtrigger@2.0-impl
else
# Audio HAL Server & Default Implementations
PRODUCT_PACKAGES += \
    android.hardware.audio@2.0-service \
    android.hardware.audio@4.0-impl \
    android.hardware.audio.effect@4.0-impl \
    android.hardware.soundtrigger@2.0-impl
endif

# AudioHAL libraries
PRODUCT_PACKAGES += \
	audio.primary.$(TARGET_SOC) \
	audio.usb.default \
	audio.r_submix.default

# AudioHAL Configurations
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/usb_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/usb_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/a2dp_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/a2dp_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/r_submix_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/r_submix_audio_policy_configuration.xml \
	device/samsung/universal9810/audio_policy_volumes_v2.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_volumes.xml \
	frameworks/av/services/audiopolicy/config/default_volume_tables.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default_volume_tables.xml \

# Mixer Path Configuration for AudioHAL
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/mixer_paths.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_paths.xml \
	device/samsung/universal9810/audio/config/audio_board_info.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_board_info.xml

# AudioEffectHAL Configuration
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/audio_effects.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_effects.xml

# Enable AAudio MMAP/NOIRQ data path.
 PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_policy=2
 PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_exclusive_policy=2
 PRODUCT_PROPERTY_OVERRIDES += aaudio.hw_burst_min_usec=2000

# Calliope firmware overwrite
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/firmware/calliope_dram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram.bin \
	device/samsung/universal9810/firmware/calliope_sram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram.bin \
	device/samsung/universal9810/firmware/calliope_iva.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_iva.bin

# Audio Device Configurations
PRODUCT_PROPERTY_OVERRIDES += \
	ro.config.num_speaker=1 \
	ro.config.num_mic=2 \
	ro.config.num_proximity=0 \
	ro.config.speaker_amp=1 \
	ro.config.bluetooth=external \
	ro.config.fmradio=external \
	ro.config.usb_by_primary=no \
	ro.config.a2dp_by_primary=no

# AudioEffectHAL library
ifeq ($(BOARD_USE_OFFLOAD_AUDIO), true)
ifeq ($(BOARD_USE_OFFLOAD_EFFECT),true)
PRODUCT_PACKAGES += \
	libexynospostprocbundle
endif
endif

# SoundTriggerHAL library
ifeq ($(BOARD_USE_SOUNDTRIGGER_HAL), true)
PRODUCT_PACKAGES += \
	sound_trigger.primary.$(TARGET_SOC)
endif

# A-Box Service Daemon
PRODUCT_PACKAGES += main_abox

# TinyTools for Audio
ifeq ($(TARGET_BUILD_VARIANT),eng)
PRODUCT_PACKAGES += \
    tinyplay \
    tinycap \
    tinymix \
    tinypcminfo
endif


# Libs
PRODUCT_PACKAGES += \
	com.android.future.usb.accessory

# Gralloc modules
ifeq ($(ENABLE_Q_FEATURE),true)
PRODUCT_PACKAGES += android.hardware.graphics.mapper@2.0-impl-2.1
else
PRODUCT_PACKAGES += android.hardware.graphics.mapper@2.0-impl
endif

PRODUCT_PACKAGES += \
    android.hardware.graphics.allocator@2.0-service \
    android.hardware.graphics.allocator@2.0-impl \
    gralloc.$(TARGET_SOC)

PRODUCT_PACKAGES += \
    memtrack.$(TARGET_BOOTLOADER_BOARD_NAME)\
    android.hardware.memtrack@1.0-impl \
    android.hardware.memtrack@1.0-service \
	libion_exynos \
	libion

PRODUCT_PACKAGES += \
    libhwjpeg

# Video Editor
PRODUCT_PACKAGES += \
	VideoEditorGoogle

# WideVine modules
PRODUCT_PACKAGES += \
	android.hardware.drm@1.0-impl \
	android.hardware.drm@1.0-service \
	android.hardware.drm@1.4-service.clearkey \
	android.hardware.drm@1.4-service.widevine

# SecureDRM modules
PRODUCT_PACKAGES += \
	secdrv \
	tlwvdrm \
	tlsecdrm \
	liboemcrypto_modular

#secdrv
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/FFFFFFFF000000000000000000000001.drbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/FFFFFFFF000000000000000000000001.drbin

#tlwvdrm
ifeq ($(ENABLE_Q_FEATURE),true)
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/api29/00060308060501020000000000000000.tabin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/00060308060501020000000000000000.tabin
else
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/00060308060501020000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/00060308060501020000000000000000.tlbin
endif

ifeq ($(ENABLE_Q_FEATURE),true)
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/tee/kinibi500

# MobiCore setup
PRODUCT_PACKAGES += \
	libMcClient \
	libMcRegistry \
	libgdmcprov \
	mcDriverDaemon \
	vendor.trustonic.tee@1.1-service \
	TeeService \
	libTeeClient \
	libteeservice_client.trustonic
else
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos9810/mobicore

# MobiCore setup
PRODUCT_PACKAGES += \
	libMcClient \
	libMcRegistry \
	libgdmcprov \
	mcDriverDaemon
endif
PRODUCT_PACKAGES += tee_whitelist
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/exynos9810/secapp

# Camera HAL
 PRODUCT_PACKAGES += \
    android.hardware.camera.provider@2.4-impl \
    android.hardware.camera.provider@2.4-service \
    camera.$(TARGET_SOC)

# Copy FIMC_IS DDK Libraries
 PRODUCT_COPY_FILES += \
    device/samsung/universal9810/firmware/camera/fimc_is_lib.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/fimc_is_lib.bin \
    device/samsung/universal9810/firmware/camera/fimc_is_rta.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/fimc_is_rta.bin \
    device/samsung/universal9810/firmware/camera/setfile_2l3.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_2l3.bin \
    device/samsung/universal9810/firmware/camera/setfile_3m3.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_3m3.bin \
    device/samsung/universal9810/firmware/camera/setfile_3h1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_3h1.bin

# Copy Camera HFD Setfiles
 PRODUCT_COPY_FILES += \
    device/samsung/universal9810/firmware/camera/libhfd/default_configuration.hfd.cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/default_configuration.hfd.cfg.json \
    device/samsung/universal9810/firmware/camera/libhfd/pp_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/pp_cfg.json \
    device/samsung/universal9810/firmware/camera/libhfd/tracker_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/tracker_cfg.json \
    device/samsung/universal9810/firmware/camera/libhfd/WithLightFixNoBN.SDNNmodel:$(TARGET_COPY_OUT_VENDOR)/firmware/WithLightFixNoBN.SDNNmodel

PRODUCT_COPY_FILES += \
	device/samsung/universal9810/handheld_core_hardware.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/handheld_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.camera.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.xml \
	frameworks/native/data/etc/android.hardware.camera.front.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.front.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.accessory.xml \
	frameworks/native/data/etc/android.hardware.audio.low_latency.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.low_latency.xml \
	frameworks/native/data/etc/android.hardware.audio.pro.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.pro.xml \

# FEATURE_OPENGLES_EXTENSION_PACK support string config file
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.opengles.aep.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.opengles.aep.xml

# vulkan version information
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.vulkan.compute-0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.compute.xml \
	frameworks/native/data/etc/android.hardware.vulkan.level-1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.level.xml \
	frameworks/native/data/etc/android.hardware.vulkan.version-1_1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.version.xml \
	frameworks/native/data/etc/android.software.vulkan.deqp.level-2019-03-01.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.vulkan.deqp.level.xml

PRODUCT_PROPERTY_OVERRIDES += \
	ro.opengles.version=196610 \
	ro.sf.lcd_density=480 \
	debug.slsi_platform=1 \
	debug.hwc.winupdate=1

# SurfaceFlinger
PRODUCT_PROPERTY_OVERRIDES += \
    ro.surface_flinger.vsync_event_phase_offset_ns=0 \
    ro.surface_flinger.vsync_sf_event_phase_offset_ns=0 \
    ro.surface_flinger.max_frame_buffer_acquired_buffers=3 \
    ro.surface_flinger.running_without_sync_framework=false \
    ro.surface_flinger.use_color_management=false\
    ro.surface_flinger.has_wide_color_display=false \
    persist.sys.sf.color_saturation=1.0 \
    debug.sf.latch_unsignaled=0 \
    debug.sf.high_fps_late_app_phase_offset_ns=0 \
    debug.sf.high_fps_late_sf_phase_offset_ns=0 \
    debug.sf.disable_backpressure=1

# hw composer HAL
PRODUCT_PACKAGES += \
	hwcomposer.$(TARGET_BOOTLOADER_BOARD_NAME)

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS5_DSS_FEATURE), true)
PRODUCT_PROPERTY_OVERRIDES += \
        ro.exynos.dss=1
endif

# set external afbc
ifeq ($(BOARD_USES_EXYNOS_AFBC_FEATURE), true)
PRODUCT_PROPERTY_OVERRIDES += \
        ro.vendor.ddk.set.afbc=1
endif

# Set default USB interface
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
	persist.sys.usb.config=mtp,adb

PRODUCT_CHARACTERISTICS := phone

PRODUCT_AAPT_CONFIG := normal hdpi xhdpi xxhdpi
PRODUCT_AAPT_PREF_CONFIG := xxhdpi

####################################
## VIDEO
####################################
# MFC firmware overwrite
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/firmware/mfc_fw_v12.0.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/mfc_fw.bin

# 1. Codec 2.0
ifeq ($(BOARD_USE_DEFAULT_SERVICE), true)
# default service
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/c2service

DEVICE_MANIFEST_FILE += \
	device/samsung/universal9810/manifest_media_c2_default.xml

PRODUCT_COPY_FILES += \
	device/samsung/universal9810/media_codecs_performance_c2.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance_c2.xml

PRODUCT_PACKAGES += \
    samsung.hardware.media.c2@1.1-default-service
endif

# 2. OpenMAX IL
# OpenMAX IL configuration files
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/media_codecs_performance.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance.xml \
	device/samsung/universal9810/media_codecs.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs.xml

PRODUCT_COPY_FILES += \
    device/samsung/universal9810/seccomp_policy/mediacodec-seccomp.policy:$(TARGET_COPY_OUT_VENDOR)/etc/seccomp_policy/mediacodec.policy
####################################

# Camera
PRODUCT_COPY_FILES += \
	device/samsung/universal9810/media_profiles.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_profiles_V1_0.xml

# Telephony
PRODUCT_COPY_FILES += \
	frameworks/av/media/libstagefright/data/media_codecs_google_telephony.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_telephony.xml

$(warning #### [WIFI] WLAN_VENDOR = $(WLAN_VENDOR))
$(warning #### [WIFI] WLAN_CHIP = $(WLAN_CHIP))
$(warning #### [WIFI] WLAN_CHIP_TYPE = $(WLAN_CHIP_TYPE))
$(warning #### [WIFI] WIFI_NEED_CID = $(WIFI_NEED_CID))
$(warning #### [WIFI] ARGET_BOARD_PLATFORM = $(ARGET_BOARD_PLATFORM))
$(warning #### [WIFI] TARGET_BOOTLOADER_BOARD_NAME = $(TARGET_BOOTLOADER_BOARD_NAME))

PRODUCT_COPY_FILES += device/samsung/universal9810/wpa_supplicant.conf:$(TARGET_COPY_OUT_VENDOR)/etc/wifi/wpa_supplicant.conf

# setup dalvik vm configs.
$(call inherit-product, frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)

PRODUCT_TAGS += dalvik.gc.type-precise

# Exynos OpenVX framework
PRODUCT_PACKAGES += \
		libexynosvision

ifeq ($(TARGET_USES_CL_KERNEL),true)
PRODUCT_PACKAGES += \
       libopenvx-opencl
endif

#GPS
# PRODUCT_PACKAGES += \
	android.hardware.gnss@1.0-impl \
	android.hardware.gnss@1.1-impl \
	vendor.samsung.hardware.gnss@1.0-impl \
	vendor.samsung.hardware.gnss@1.0-service

PRODUCT_PACKAGES += \
	android.hardware.gnss@1.0-impl \
	android.hardware.gnss@1.0-service \
	gps.$(TARGET_SOC)

#Gatekeeper
 PRODUCT_PACKAGES += \
	android.hardware.gatekeeper@1.0-impl \
	android.hardware.gatekeeper@1.0-service \
	gatekeeper.$(TARGET_SOC)

# gatekeeper TA
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/08130000000000000000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/08130000000000000000000000000000.tlbin

ifeq ($(BOARD_SUPPORT_Gatekeeper),true)
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos9810/mobicore/TlcTeeGatekeeper
endif

# KeyManager/AES modules
ifeq ($(ENABLE_Q_FEATURE),true)
PRODUCT_PACKAGES += \
	tlkeyman_api29
#tlkeyman_api29
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/api29/FFFFFFFFD00000000000000000000016.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/FFFFFFFFD00000000000000000000016.tlbin
else
PRODUCT_PACKAGES += \
	tlkeyman
#tlkeyman
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/FFFFFFFFD00000000000000000000016.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/FFFFFFFFD00000000000000000000016.tlbin
endif

PRODUCT_PACKAGES += \
    android.hardware.graphics.composer@2.2-impl \
    android.hardware.graphics.composer@2.2-service \
    vendor.samsung_slsi.hardware.ExynosHWCServiceTW@1.0-service

PRODUCT_PACKAGES += \
	android.hardware.renderscript@1.0-impl

ifeq ($(ENABLE_Q_FEATURE),true)
PRODUCT_PROPERTY_OVERRIDES += \
	ro.frp.pst=/dev/block/by-name/frp
else
PRODUCT_PROPERTY_OVERRIDES += \
	ro.frp.pst=/dev/block/by-name/persist
endif

# RenderScript HAL
PRODUCT_PACKAGES += \
	android.hardware.renderscript@1.0-impl

#VNDK
ifndef BOARD_VNDK_VERSION
PRODUCT_PACKAGES += \
	vndk-sp
endif

PRODUCT_ENFORCE_RRO_TARGETS := \
	framework-res

# Keymaster
ifeq ($(ENABLE_Q_FEATURE),true)
PRODUCT_PACKAGES += \
	android.hardware.keymaster@4.0-impl \
	android.hardware.keymaster@4.0_tee-service
#tlkeymaster_api29
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/api29/0706000000000000000000000000004d.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/0706000000000000000000000000004d.tlbin
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/tee/TlcTeeKeymaster4
else
PRODUCT_PACKAGES += \
	android.hardware.keymaster@3.0-impl \
	android.hardware.keymaster@3.0-service
#tlkeymasterM
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/0706000000000000000000000000004d.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/0706000000000000000000000000004d.tlbin
endif

PRODUCT_PACKAGES += \
	keystore.$(TARGET_SOC)

ifeq ($(BOARD_SUPPORT_KeymasterM),true)
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos9810/mobicore/TlcTeeKeymasterM
endif

ifneq ($(MALI_ON_KEYSTONE),true)
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/exynos9810/libs/r32p1

PRODUCT_PACKAGES += \
	libGLES_mali \
	libOpenCL_prebuilt \
	libOpenCL_symlink \
	vulkan.mali_prebuilt \
	libgpudataproducer

PRODUCT_PROPERTY_OVERRIDES += ro.hardware.vulkan=mali
endif

#Enable supporting AGI(Android GPU Inspector)
PRODUCT_PROPERTY_OVERRIDES += \
	graphics.gpu.profiler.support=true

#vendor directory packages
PRODUCT_PACKAGES += \
	whitelist \
	libstagefright_hdcp \
	libskia_opt

# PRODUCT_PACKAGES += \
	mfc_fw.bin \
	calliope_sram.bin \
	calliope_dram.bin \
	calliope_iva.bin \
	vts.bin \
	dsm.bin \
	APBargeIn_AUDIO_SLSI.bin \
	AP_AUDIO_SLSI.bin \
	APBiBF_AUDIO_SLSI.bin \
	APDV_AUDIO_SLSI.bin

# Device specific services.
# PRODUCT_PACKAGES += \
# 	exynos9810-services

# PRODUCT_SYSTEM_SERVER_JARS += \
# 	exynos9810-services

$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)
$(call inherit-product, hardware/samsung_slsi/exynos5/exynos5.mk)
$(call inherit-product-if-exists, hardware/samsung_slsi/exynos9810/exynos9810.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/common/exynos-vendor.mk)
# $(call inherit-product, device/samsung/universal9810/telephony_binaries/telephony_binaries.mk)
# $(call inherit-product, device/samsung/universal9810/gnss_binaries/gnss_binaries.mk)

# GMS Applications
ifeq ($(WITH_GMS),true)
GMS_ENABLE_OPTIONAL_MODULES := true
USE_GMS_STANDARD_CONFIG := true
$(call inherit-product-if-exists, vendor/partner_gms/products/gms.mk)
endif

# hw composer property : Properties related to hwc will be defined in hwcomposer_property.mk
$(call inherit-product-if-exists, hardware/samsung_slsi/graphics/base/hwcomposer_property.mk)
